<?php

class Database extends PDO {

	public function __construct() {
		try {

		parent::__construct();
		parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch (Exception $ex) {
			//echo $ex->getMessage();
			die("Error de conexiòn a base de datos");
		}
	}
}
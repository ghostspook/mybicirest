<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

ini_set('display_errors', 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';
include "NotORM.php";

// *** Activar esta líneas en desarrollo
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);
//*** Fin de activar estas líneas en desarrollo


// **** Activar esta línea en producción
//$app = new \Slim\App;
// **** Fin de activar esta línea en producción

/* Database Configuration */
// $dbhost   = '192.168.43.91';
$dbuser   = 'mybiciusr';
$dbpass   = 'mybiciusr';
// $dbname   = 'mybici';
$dbmethod = 'mysql:dbname=';

/* Creando PDO */
//$dsn = $dbmethod.$dbname.';dbhost='.$dbhost;
$pdo = new PDO('mysql:host=127.0.0.1;dbname=mybici', $dbuser, $dbpass,
    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
$db = new NotORM($pdo);

function getConnection() {
    try {
        //$db_username = "mybiciusr";
        //$db_password = "mybiciusr";
        //$conn = new PDO("mysql:host=localhost;dbname=mybici", $db_username, $db_password);
        //$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $db_username = "mybiciusr";
        $db_password = "mybiciusr";
        $conn = new PDO("mysql:host=127.0.0.1;dbname=mybici", $db_username, $db_password,
        	array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
    } catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
    return $conn;
}

$app->get('/hello/{name}', function (Request $request, Response $response) {
	$name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});


$app->post('/registro', function (Request $request, Response $response) {

	$body = $request->getBody();
	$params = json_decode($body);
	$username = $params->user;
	$password = $params->password;
	$hash = md5($password);
	$token = bin2hex(openssl_random_pseudo_bytes(16));

	$conn = getConnection();

	$statement = $conn->prepare("SELECT * FROM USUARIO WHERE nombre=:nombre");
	$statement->execute(array(
		"nombre" => $username
		));
	$row = $statement->fetch(PDO::FETCH_ASSOC);

	if ($row) {
		$respuesta = array(
			"resultado" => "error",
			"error" => 1,
			"msjError" => "Usuario ya existe",
			"usuario" => $username,
			"token" => ""
			);
	} else {
		$statement = $conn->prepare("INSERT INTO USUARIO(nombre, contrasena, TIPO_id, ESTADO_id, token)
		    VALUES(:nombre, :contrasena, :tipo, :estado, :token)");
		$statement->execute(array(
		    "nombre" => $username,
		    "contrasena" => "$hash",
		    "tipo" => "2",
		    "estado" => "1",
		    "token" => $token
		));

		$respuesta = array(
			"resultado" => "ok",
			"error" => 0,
			"msjError" => "",
			"usuario" => $username,
			"token" => $token
			);
	}


	header("Content-Type: application/json");
	$response->getBody()->write(json_encode($respuesta));
});

$app->post('/token', function (Request $request, Response $response) {

	$body = $request->getBody();
	$params = json_decode($body);
	$token = $params->token;

	$conn = getConnection();

	$statement = $conn->prepare("SELECT nombre FROM USUARIO WHERE token=:token");
	$statement->execute(array(
		"token" => $token
		));
	$row = $statement->fetch(PDO::FETCH_OBJ);

	if ($row) {
		$respuesta = array(
			"resultado" => "ok",
			"error" => 0,
			"msjError" => "",
			"user" => $row->nombre
			);
	} else {
		$respuesta = array(
			"resultado" => "error",
			"error" => 2,
			"msjError" => "Token no encontrado. Inicie sesión de nuevo.",
			"user" => "",
			);
	}


	header("Content-Type: application/json");
	$response->getBody()->write(json_encode($respuesta));
});

$app->post('/login', function (Request $request, Response $response) {

	$body = $request->getBody();
	$params = json_decode($body);
	$username = $params->user;
	$password = $params->password;
	$hash = md5($password);

	$conn = getConnection();

	$statement = $conn->prepare("SELECT nombre FROM USUARIO WHERE nombre=:username AND contrasena=:password");
	$statement->execute(array(
		"username" => $username,
		"password" => $hash
		));
	$row = $statement->fetch(PDO::FETCH_ASSOC);

	if ($row) {
		$token = bin2hex(openssl_random_pseudo_bytes(16));

		$statement = $conn->prepare("UPDATE USUARIO SET token = :token WHERE nombre=:username");
		$statement->execute(array(
		    "username" => $username,
		    "token" => $token
		));

		$respuesta = array(
			"resultado" => "ok",
			"error" => 0,
			"msjError" => "",
			"token" => $token
			);
	} else {
		$respuesta = array(
			"resultado" => "error",
			"error" => 3,
			"msjError" => "Error de autenticación.",
			"token" => "",
			);
	}

	header("Content-Type: application/json");
	$response->getBody()->write(json_encode($respuesta));
});

$app->get('/test', function (Request $request, Response $response) {
	$conn = getConnection();

	$token = bin2hex(openssl_random_pseudo_bytes(16));
    $response->getBody()->write($token);

    return $response;
});

$app->get('/estaciones', function(Request $request, Response $response) use($db){
    $estaciones = array();
    foreach ($db->PUESTO_ALQUILER() as $estacion) {
    	$parqueosLlenos = $db->ESTACIONAMIENTO()->where('PUESTO_ALQUILER_id', $estacion['id'])->where('ESTADO_id', 5);
        
    	$cuentaBicicletasDisponibles = 0;
    	foreach ($parqueosLlenos as $parqueoOcupado) {
    		$bicicleta = $db->BICICLETA()->where('id', $parqueoOcupado['BICICLETA_id'])->fetch();
    		if ($bicicleta['ESTADO_id'] == 7 ){
    			$cuentaBicicletasDisponibles = $cuentaBicicletasDisponibles + 1;
    		}
    	}

        $item  = array(
            'codigo' => $estacion['codigo'],
            'id' => $estacion['id'],
            'latitud' => $estacion['latitud'],
            'longitud' => $estacion['longitud'],
            'nombre' => $estacion['nombre'],
			'parqueosLlenosCount' => count($parqueosLlenos),
            'parqueosVaciosCount' => count($db->ESTACIONAMIENTO()->
            		where('PUESTO_ALQUILER_id', $estacion['id'])->where('ESTADO_id', 4)),
            'bicicletasDisponiblesCount' => $cuentaBicicletasDisponibles
        );
        array_push($estaciones, $item);
    }
    header("Content-Type: application/json");
    $response->getBody()->write(json_encode($estaciones, JSON_FORCE_OBJECT));
});

$app->post('/tickets/reserva', function(Request $request, Response $response) use($db) {
	$body = $request->getBody();
	$params = json_decode($body);
	$username = $params->user;
	$estacionOrigenId = $params->estacionOrigenId;
	$estacionDestinoId = $params->estacionDestinoId;


	$user = $db->USUARIO()->where('nombre', $username)->fetch();
	if (!$user) {
		$objeto = array(
            'error' => 3,
            'message' => "Usuario no existe"
        );
	} 
	else if (!tieneTicketVigente($user['id'], $db)) {
		$userId = $user['id'];
		$bicicleta = getBicicletaDisponible($estacionOrigenId, $db);
		$bicicletaId = $bicicleta['id'];
		if (!$bicicleta) {
			$objeto = array(
        		'error' => 1,
        		'message' => "No hay bicicletas disponibles"
    		);
		} else {
			$bicicletaObj = $db->BICICLETA()->where('id', $bicicletaId);
			$cambios = array('ESTADO_id' => "9"); //En reserva
			$result = $bicicletaObj->update($cambios);
			if ($result != 1) {
				$objeto = array(
	        		'error' => 4,
	        		'message' => "Bicicleta no pudo reservarse"
	    		);
			} else {
				$parqueosDisponiblesDestino = $db->ESTACIONAMIENTO()->
						where('PUESTO_ALQUILER_id', $estacionDestinoId)->
						where('ESTADO_id', 4)->fetch();
				if (!$parqueosDisponiblesDestino){
					$objeto = array(
		        		'error' => 4,
		        		'message' => "No hay parqueo disponible en destino"
		    		);
				} else {
					$parqueoOrigen = $db->ESTACIONAMIENTO()->
							where('BICICLETA_id', $bicicletaId)->
							where('PUESTO_ALQUILER_id', $estacionOrigenId)->fetch();
					$parqueoId = $parqueoOrigen['id'];
					$ticketObj = $db->TICKET();
					$now = new NotORM_Literal("NOW()");
					$data = array(
						'TIPO_id' => 3,
						'USUARIO_id' => $userId,
						'BICICLETA_id' => $bicicletaId,
						'origen_puesto_alquiler' => $estacionOrigenId,
						'origen_estacionamiento' => $parqueoId,
						'destino_puesto_alquiler' => $estacionDestinoId,
						'fecha' => $now,
						'hora_creacion' => $now,
						'ESTADO_id' => 10 //Generado
					);
					$result = $ticketObj->insert($data);
					if (!$result) {
						$objeto = array(
			        		'error' => 2,
			        		'message' => "No se pudo generar el ticket"
			    		);
					} else {
						$ticketId = $result['id'];
						$objeto = getTicketForResponse($ticketId, $db);
					}
				}
			}
		}
	} else {
		$objeto = array(
        		'error' => 5,
        		'message' => "Existe una reserva vigente"
    		);
	}

    header("Content-Type: application/json");
    $response->getBody()->write(json_encode($objeto, JSON_FORCE_OBJECT));
});

function tieneTicketVigente($userId, $db) {
	$tickets = $db->TICKET()->where('USUARIO_id', $userId)->
			where('ESTADO_id = 10 OR ESTADO_id = 11');
	if (count($tickets) > 0) return true; else return false;
}

$app->post('/tieneTicketVigente', function(Request $request, Response $response) use($db) {
	$body = $request->getBody();
	$params = json_decode($body);
	$username = $params->user;

	$user = $db->USUARIO()->where('nombre', $username)->fetch();
 	$response->getBody()->write(tieneTicketVigente($user['id'], $db));
	header("Content-Type: application/json");
});

function getBicicletaDisponible($estacionOrigenId, $db) {
	$parqueosLlenosObj = $db->ESTACIONAMIENTO()->
			where('PUESTO_ALQUILER_id', $estacionOrigenId)->
			where('ESTADO_id', 5)->
			order('codigo');
	foreach ($parqueosLlenosObj as $parqueo) {
		$bicicleta = $db->BICICLETA()->where('id', $parqueo['BICICLETA_id'])->fetch();
		if ($bicicleta['ESTADO_id'] == 7) {
			return $bicicleta;
		}
	}

	return false;
}

$app->post('/getBicicletaDisponible', function(Request $request, Response $response) use($db) {
	$body = $request->getBody();
	$params = json_decode($body);
	$estacionOrigenId = $params->estacionOrigenId;

 	$response->getBody()->write(json_encode(getBicicletaDisponible($estacionOrigenId, $db), JSON_FORCE_OBJECT));
	header("Content-Type: application/json");
});

$app->post('/tickets/mistickets', function(Request $request, Response $response) use($db) {
	$body = $request->getBody();
	$params = json_decode($body);
	$username = $params->user;

	$user = $db->USUARIO()->where('nombre', $username)->fetch();
    
    $tickets = $db->TICKET()->where('USUARIO_id', $user['id'])->where('ESTADO_id <> ?', 13)->order('id');
    $objeto = array();
    foreach ($tickets as $ticket) {
    	$objeto[] = getTicketForResponse($ticket['id'], $db);
    }

    header("Content-Type: application/json");
    $response->getBody()->write(json_encode($objeto, JSON_FORCE_OBJECT));
});

$app->get('/tickets/{ticketId}', function(Request $request, Response $response) use($db) {
	$ticketId = $request->getAttribute('ticketId');

 	$response->getBody()->write(json_encode(getTicketForResponse($ticketId, $db), JSON_FORCE_OBJECT));
	header("Content-Type: application/json");
});

function getTicketForResponse($ticketId, $db) {
	$ticketObj = $db->TICKET()->where('id', $ticketId);
	$ticket = $ticketObj->fetch();
	$estacionBicicleta = $db->PUESTO_ALQUILER()->where('id',$ticket->BICICLETA['PUESTO_ALQUILER_id'])->fetch();
	$estacionOrigen = $db->PUESTO_ALQUILER()->where('id',$ticket['origen_puesto_alquiler'])->fetch();
	$estacionDestino = $db->PUESTO_ALQUILER()->where('id',$ticket['destino_puesto_alquiler'])->fetch();
	$bicicleta = $db->BICICLETA()->where('id',$ticket['BICICLETA_id'])->fetch();
	$parqueoOrigen = $db->ESTACIONAMIENTO()->where('id', $ticket['origen_estacionamiento'])->fetch();
	if($ticket){
        $objeto = array(
            'id' => $ticket['id'],
            'tipoId' => $ticket['TIPO_id'],
            'usuarioId' => $ticket['USUARIO_id'],
            'bicicletaId' => $ticket['BICICLETA_id'],
            'bicicletaCodigo' => $estacionBicicleta['codigo'].'B'.$bicicleta['codigo'],
            'estacionOrigenId' => $ticket['origen_puesto_alquiler'],
            'estacionOrigenNombre' => $estacionOrigen['nombre'],
            'estacionOrigenParqueoId' => $ticket['origen_estacionamiento'],
            'estacionDestinoId' => $ticket['destino_puesto_alquiler'],
            'estacionDestinoNombre' => $estacionDestino['nombre'],
            'estacionDestinoParqueoId' => $ticket['destino_estacionamiento'],
            'fecha' => $ticket['fecha'],
            'horaCreacion' => $ticket['hora_creacion'],
            'horaLimiteRetiro' => date('H:i:s',strtotime($ticket['hora_creacion']) + 900), //900 segundos = 15 minutos
            'horaRetiro' => $ticket['hora_retiro'],
            'horaEntrega' => $ticket['hora_entrega'],
            'duracion' => $ticket['duracion'],
            'estadoId' => $ticket['ESTADO_id'],
            'parqueoOrigenCodigo' => $estacionOrigen['codigo'].'P'.$parqueoOrigen['codigo']
        );
    }
    else{
        $objeto = array(
            'status' => false,
            'message' => "No se encontró el registro"
        );
    }
    return $objeto;
}

$app->put('/tickets/anular/{ticket}', function(Request $request, Response $response) use($db) {
	$id = $request->getAttribute('ticket');
	
	$ticket = $db->TICKET()->where('id', $id);
	$data = $ticket->fetch();

	if ($data) {
		//Anula ticket
		$cambios = array('ESTADO_id' => "13"); //anulado
		$result = $ticket->update($cambios);

		//Libera bicicleta
		$bicicleta = $db->BICICLETA()->where('id', $data['BICICLETA_id']);
		$cambiosBicicleta = array('ESTADO_id' => "7"); //Lista para ser usada de nuevo
		$result = $bicicleta->update($cambiosBicicleta);

		$objeto = array(
			'status' => "1",
			"message" => "Ticket anulado"
			);
	} else {
		$objeto = array(
			'status' => false,
			"message" => "Ticket no encontrado"
			);
	}

    header("Content-Type: application/json");
    $response->getBody()->write(json_encode($objeto, JSON_FORCE_OBJECT));
});

$app->run();